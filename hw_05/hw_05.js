"use strict";

/**
 Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.

 Технічні вимоги:
 При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій.
 Для цього потрібно надіслати GET запит на наступні дві адреси:
 https://ajax.test-danit.com/api/json/users
 https://ajax.test-danit.com/api/json/posts

 Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.

 Кожна публікація має бути відображена у вигляді картки (приклад: https://prnt.sc/q2em0x), та включати заголовок,
 текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
 На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки.
 При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}.
 Після отримання підтвердження із сервера (запит пройшов успішно),
 картку можна видалити зі сторінки, використовуючи JavaScript.
 Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.

 Цей сервер є тестовим. Після перезавантаження сторінки всі зміни,
 які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
 Картки обов'язково мають бути реалізовані у вигляді ES6 класів.
 Для цього необхідно створити клас Card. При необхідності ви можете додавати також інші класи.


 Необов'язкове завдання підвищеної складності
 Поки з сервера під час відкриття сторінки завантажується інформація, показувати анімацію завантаження.
 Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
 Додати зверху сторінки кнопку Додати публікацію.
 При натисканні на кнопку відкривати модальне вікно, в якому користувач зможе ввести заголовок та текст публікації.
 Після створення публікації дані про неї необхідно надіслати в POST
 запиті на адресу: https://ajax.test-danit.com/api/json/posts.
 Нова публікація має бути додана зверху сторінки (сортування у зворотному хронологічному порядку).
 Автором можна присвоїти публікації користувача з id: 1.
 Додати функціонал (іконку) для редагування вмісту картки.
 Після редагування картки для підтвердження
 змін необхідно надіслати PUT запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}.
 */
const usersUrl = "https://ajax.test-danit.com/api/json/users"
const postsUrl = "https://ajax.test-danit.com/api/json/posts"

// fetch(postsUrl)
//     .then( res => res.json() )
//     .then(res => console.log(res));
// fetch(usersUrl)
//     .then( res => res.json() )
//     .then(res => console.log(res));

function createHtmlCard(postTitle, postBody, usersName, email, postId, userImg) {
    const divContainer = document.createElement('div');
    divContainer.setAttribute('class', 'card-container');
    divContainer.setAttribute('id', `post-${postId}`);
    const title = document.createElement('h3');
    title.setAttribute('class', 'card-title');
    const text = document.createElement('p');
    text.setAttribute('class', 'card-text');
    title.textContent = postTitle;
    text.textContent = postBody;
    const userName = document.createElement('p');
    const userEmail = document.createElement('a');
    userName.setAttribute('class', 'user-name');
    userEmail.setAttribute('class', 'user-email');
    userEmail.setAttribute('href', `${email}`);
    userName.textContent = usersName;
    userEmail.textContent = email;
    const imgContainer = document.createElement('div');
    imgContainer.setAttribute('class', 'card-img-container');
    const img = document.createElement('img');
    img.setAttribute('alt', 'user photo');
    if (userImg === undefined) {
        img.setAttribute('src', '#');
    }
    imgContainer.append(img);
    const delIcon = document.createElement('i');
    delIcon.setAttribute('class', 'fa-regular fa-circle-xmark del-icon');
    delIcon.setAttribute('id', `${postId}`);
    const linksContainer = document.createElement('div');
    linksContainer.setAttribute('class', 'links-container');
    linksContainer.innerHTML = '<a href="#"><i class="fa-regular fa-comment"></i></a>' +
        '<a href="#"><i class="fa-solid fa-repeat"></i></a>' +
        '<a href="#"><i class="fa-regular fa-heart"></i></a>'
    const textContainer = document.createElement('div');
    textContainer.setAttribute('class', 'text-container');
    textContainer.append(userName, userEmail, title, text, linksContainer);
    divContainer.append(imgContainer, textContainer, delIcon);
    document.body.prepend(divContainer);
}

class Card {
    constructor(postTitle, postBody, userId, id) {
        this._postTitle = postTitle
        this._postBody = postBody
        this._userId = userId
        this._id = id
    }

    addPost() {
        fetch(usersUrl)
            .then(res => res.json())
            .then(res => {
                res.forEach(el => {
                    if (this._userId === el.id) {
                        createHtmlCard(this._postTitle, this._postBody, el.name, el.email, this._id, el.img);
                    }
                })
            });
    }
}

fetch(postsUrl)
    .then(res => res.json())
    .then(res => res.forEach(el => {
        el = new Card(el.title, el.body, el.userId, el.id);
        el.addPost();
    }));

document.body.addEventListener('click', e => {
    if (e.target.classList.contains("del-icon")) {
        const id = e.target.getAttribute('id');

        fetch(postsUrl + "/" + id, {
            method: 'DELETE',
        })
            .then(response => {
                try {
                    if (response.status > 300 || response.ok === false) {
                        throw new Error("Can`t delete data!");
                    }
                    console.log(response);
                    const removedPost = document.querySelector(`#post-${id}`);
                    removedPost.remove();
                } catch (err) {
                    console.log(err.message);
                }
            });
    }
})