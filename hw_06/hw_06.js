"use strict";

/**
 Теоретичне питання
 Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

 Завдання
 Написати програму "Я тебе знайду по IP"

 Технічні вимоги:
 Створити просту HTML-сторінку з кнопкою Знайти по IP.
 Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json,
 отримати звідти IP адресу клієнта.
 Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/
 та отримати інформацію про фізичну адресу.
 під кнопкою вивести на сторінку інформацію,
 отриману з останнього запиту – континент, країна, регіон, місто, район.
 Усі запити на сервер необхідно виконати за допомогою async await.

 */

document.querySelector('.btn').addEventListener('click', async () => {
    let res = await fetch('https://api.ipify.org/?format=json');
    let result = await res.json();
    let response = await fetch("http://ip-api.com/json/" + result.ip + "?fields=1572893");
    let address = await response.json();
    const ul = document.createElement('ul');
    document.body.append(ul);
    for (const key in address) {
        if (address[key] === ""){
            address[key] = "no data";
        }
        ul.innerHTML += `<li>${key}: ${address[key]}</li>`
    }
})