"use strict"

/**
 - Виведіть цей масив на екран у вигляді списку
 (тег ul – список має бути згенерований за допомогою Javascript).
 - На сторінці повинен знаходитись div з id="root",
 куди і потрібно буде додати цей список
 (схоже завдання виконувалось в модулі basic).
 - Перед додаванням об'єкта на сторінку потрібно
 перевірити його на коректність
 (в об'єкті повинні міститися всі три властивості - author, name, price).
 Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка
 із зазначенням - якої властивості немає в об'єкті.
 - Ті елементи масиву, які не є коректними за умовами попереднього пункту,
 не повинні з'явитися на сторінці.
 */

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const div = document.getElementById('root');
const list = document.createElement('ul');
list.style.cssText = 'color: #0000ff; max-width: 300px; margin: 0 auto';
div.append(list);


class ListBuild {
    #arr = [];
    constructor(arr) {
        this.arr = arr;
    }
    set arr (arr) {
        try {
            if (!Array.isArray(arr)){
               throw new Error("Wrong data type!");
            }
            this.#arr = arr;
        } catch (err) {
            console.log(err.message);
        }
    }
    get arr () {
        return this.#arr;
    }
    createItem () {
        this.arr.forEach(element => {
            try {
                if (typeof element !== 'object' || element === null){
                    throw new Error("Data type is not a Object")
                }
                try {
                    if (element.name && element.author && element.price){
                        const li = document.createElement('li');
                        li.innerHTML = `<li> назва: ${element.name} <ul><li>автор: ${element.author}</li><li>ціна: ${element.price}</li></ul></li>`;
                        list.append(li);
                    }
                    if (!element.name) {
                        throw new Error(`Книга автора: ${element.author} не містить назви`);
                    }
                    if (!element.author) {
                        throw new Error(`Книга: "${element.name}" не містить дані про автора`);
                    }
                    if (!element.price) {
                        throw new Error(`Не вказано ціну книги: "${element.name}"`);
                    }
                } catch (e) {
                    console.log(e.message);
                }
            } catch (e) {
                console.log(e.message);
            }
        });
    }
}

const bookList1 = new ListBuild(books);
bookList1.createItem();