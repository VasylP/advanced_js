"use strict";

/**
 Отримати список фільмів серії Зоряні війни та вивести
 на екран список персонажів для кожного з них.

 Технічні вимоги:
 Надіслати AJAX запит на адресу
 https://ajax.test-danit.com/api/swapi/films
 та отримати список усіх фільмів серії Зоряні війни

 Для кожного фільму отримати з сервера список персонажів,
 які були показані у цьому фільмі. Список персонажів можна
 отримати з властивості characters.

 Як тільки з сервера буде отримана інформація про фільми,
 відразу вивести список усіх фільмів на екрані.
 Необхідно вказати номер епізоду, назву фільму,
 а також короткий зміст (поля episodeId, name, openingCrawl).

 Як тільки з сервера буде отримано інформацію про персонажів
 будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

 Необов'язкове завдання підвищеної складності
 Поки завантажуються персонажі фільму,
 прокручувати під назвою фільму анімацію завантаження.
 Анімацію можна використовувати будь-яку.
 Бажано знайти варіант на чистому CSS без використання JavaScript.
 */

const url = "https://ajax.test-danit.com/api/swapi/films";
//
//
// document.body.addEventListener('click', event => {
//     if (event.target.classList.contains("get-films")) {
//         const ul = document.createElement('ul');
//         document.body.append(ul);
//         fetch( url )
//             .then( res => res.json() )
//             .then( res => ul.innerHTML = res.map(item => `<li id="${item.id}">name: "${item.name}" <p>episode: ${item.episodeId}</p><p>${item.openingCrawl}</p>
//  <button type = "button" class="get-characters" id="${item.id}">Персонажі</button></li>`).join(''));
//         document.querySelector(".get-films").remove();
//     }
//     if (event.target.classList.contains("get-characters")) {
//         event.target.classList.toggle('active');
//         const li = document.getElementById(event.target.getAttribute('id'));
//         const ul = document.createElement('ul');
//         li.append(ul);
//         fetch( url )
//             .then( res => res.json() )
//             .then( res => {
//                 res.forEach(el => {
//                     if (+event.target.getAttribute('id') === el.id){
//                         el.characters.forEach(element => {
//                             const item = document.createElement('li');
//                             ul.append(item);
//                             fetch( element )
//                                 .then( res => res.json() )
//                                 .then( res => {
//                                     item.innerText = res.name;
//                             });
//                         })
//                     }
//                 })
//             });
//         document.querySelector(".active").remove();
//     }
// });

fetch(url)
    .then(res => res.json())
    .then(res => {
        const ul = document.createElement('ul');
        document.body.append(ul);
        ul.innerHTML = res.map(item => `<li id="${item.id}">name: "${item.name}" <p>episode: ${item.episodeId}</p><p>${item.openingCrawl}</p></li>`).join('')
    });

fetch(url)
    .then(res => res.json())
    .then(res => {
        res.forEach(el => {
            const episode = document.getElementById(el.id);
            const ulCharacters = document.createElement('ul');
            episode.append(ulCharacters);
            el.characters.forEach(element => {
                const item = document.createElement('li');
                ulCharacters.append(item);
                fetch(element)
                    .then(res => res.json())
                    .then(res => {
                        item.innerText = res.name
                    })
            })
        })
    })