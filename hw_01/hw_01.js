"use strict"

/**
                         Теоретичне питання
1. Поясніть своїми словами, як ви розумієте,
 як працює прототипне наслідування в Javascript
2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
                       Відповідь
 1. Пототипне наслідування - це можливість створення нового об'єкту який
 може унаслідувати методи від батьківського об'єкту (класу).
 2. super() використовується для виклику функцій, які належать до батьківського об'єкту.


    Завдання
    Створити клас Employee, у якому будуть такі
 характеристики - name (ім'я), age (вік), salary (зарплата).
 Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
    Створіть гетери та сеттери для цих властивостей.
    Створіть клас Programmer, який успадковуватиметься від класу Employee,
 і який матиме властивість lang (список мов).
Для класу Programmer перезапишіть гетер для властивості salary.
 Нехай він повертає властивість salary, помножену на 3.
Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

 Примітка
Завдання має бути виконане на чистому Javascript
 без використання бібліотек типу jQuery або React.
 */

class Employee {

    constructor(name, age, salary) {
        this._name = name
        this._age = age
        this._salary = salary
    }
    set name(name) {
        this._name = name;
    }

    get name() {
        return this._name;
    }

    set age(age) {
        this._age = age;
    }

    get age() {
        return this._age;
    }

    set salary(salary) {
        this._salary = salary;
    }

    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee {

    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }
    set lang(lang) {
        this._lang = lang;
    }

    get lang() {
        return this._lang;
    }

    get salary() {
        return this._salary * 3;
    }
}

const mark = new Programmer("Mark", 32, 1000, ["de", "en", "uk"]);

console.log(mark);
console.log(mark.salary);

const vlad = new Programmer("Vlad", 25, 2000, ["en", "uk"]);

console.log(vlad);
console.log(vlad.salary);

const alex = new Programmer("Alex", 33, 5000, "uk");

console.log(alex);
console.log(alex.salary);